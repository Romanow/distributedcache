package ru.romanow.distributedcache.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Created by ronin on 05.12.15
 */
@Service
public class CacheTestServiceImpl
        implements CacheTestService {

    @Override
    @Cacheable("testCache")
    public String add(String str) {
        return str + new Random(100).nextInt();
    }
}
