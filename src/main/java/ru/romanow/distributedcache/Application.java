package ru.romanow.distributedcache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.romanow.distributedcache.service.CacheTestService;

import java.util.stream.IntStream;

/**
 * Created by ronin on 24.11.15
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {
        CacheConfiguration.class,
        RedisConfiguration.class
})
public class Application
        implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    @Autowired
    private CacheTestService cacheTestService;

    @Override
    public void run(String... args) throws Exception {
        for (int ignored : IntStream.range(1, 10).toArray()) {
            String msg = cacheTestService.add("test");
            logger.info(msg);
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
