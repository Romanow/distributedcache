package ru.romanow.distributedcache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.JacksonJsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import ru.romanow.distributedcache.cache.model.event.BaseChangeEvent;
import ru.romanow.distributedcache.cache.receiver.CacheChangesReceiver;

/**
 * Created by ronin on 14.12.15
 */
@Component
public class RedisConfiguration {

    @Value("${cache.topic.name:cache.messaging}")
    private String topicName;

    @Bean
    @Autowired
    public RedisTemplate<String, Object> redisTemplate(
            RedisConnectionFactory redisConnectionFactory,
            StringRedisSerializer stringRedisSerializer,
            RedisSerializer<BaseChangeEvent> cacheDataRedisSerializer) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setValueSerializer(cacheDataRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        return redisTemplate;
    }

    @Bean
    public StringRedisSerializer stringRedisSerializer() {
        return new StringRedisSerializer();
    }

    @Bean
    @Autowired
    public RedisMessageListenerContainer container(
            RedisConnectionFactory connectionFactory,
            MessageListenerAdapter listenerAdapter,
            ChannelTopic channelTopic) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapter, channelTopic);
        return container;
    }

    @Bean
    @Autowired
    public MessageListenerAdapter listenerAdapter(
            RedisSerializer<BaseChangeEvent> cacheDataRedisSerializer,
            CacheChangesReceiver cacheChangesReceiver) {
        MessageListenerAdapter adapter =
                new MessageListenerAdapter(cacheChangesReceiver, "receiveMessage");
        adapter.setSerializer(cacheDataRedisSerializer);
        return adapter;
    }

    @Bean
    public RedisSerializer<BaseChangeEvent> cacheDataRedisSerializer() {
        return new JacksonJsonRedisSerializer<>(BaseChangeEvent.class);
    }

    @Bean
    public ChannelTopic channelTopic() {
        return new ChannelTopic(topicName);
    }
}
