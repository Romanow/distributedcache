package ru.romanow.distributedcache.cache.model.event;

import com.google.common.base.MoreObjects;
import org.codehaus.jackson.annotate.JsonIgnore;
import ru.romanow.distributedcache.cache.model.action.PutAction;

/**
 * Created by ronin on 14.12.15
 */
public class PutEvent
        extends BaseChangeEvent {
    private static final long serialVersionUID = 7229933352974754433L;

    private String key;
    private Object value;

    public PutEvent() {}

    public PutEvent(String cacheName, String key, Object value) {
        super(cacheName);
        this.key = key;
        this.value = value;
    }

    @Override
    @JsonIgnore
    public PutAction getAction() {
        return new PutAction();
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("cacheName", cacheName)
                          .add("key", key)
                          .toString();
    }
}
