package ru.romanow.distributedcache.cache.model.event;

import com.google.common.base.MoreObjects;
import org.codehaus.jackson.annotate.JsonIgnore;
import ru.romanow.distributedcache.cache.model.action.PutIfAbsentAction;

/**
 * Created by ronin on 16.12.15
 */
public class PutIfAbsentEvent
        extends BaseChangeEvent {
    private static final long serialVersionUID = -2110391687126743713L;

    private String key;
    private Object value;

    public PutIfAbsentEvent() {}

    public PutIfAbsentEvent(String cacheName, String key, Object value) {
        super(cacheName);
        this.key = key;
        this.value = value;
    }

    @Override
    @JsonIgnore
    public PutIfAbsentAction getAction() {
        return new PutIfAbsentAction();
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("cacheName", cacheName)
                          .add("key", key)
                          .toString();
    }
}
