package ru.romanow.distributedcache.cache.model.event;

import com.google.common.base.MoreObjects;
import org.codehaus.jackson.annotate.JsonIgnore;
import ru.romanow.distributedcache.cache.model.action.EvictAction;

/**
 * Created by ronin on 15.12.15
 */
public class EvictEvent
        extends BaseChangeEvent {
    private static final long serialVersionUID = -2819078581897445932L;

    private String key;

    public EvictEvent() {}

    public EvictEvent(String cacheName, String key) {
        super(cacheName);
        this.key = key;
    }

    @Override
    @JsonIgnore
    public EvictAction getAction() {
        return new EvictAction();
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("cacheName", cacheName)
                          .add("key", key)
                          .toString();
    }
}
