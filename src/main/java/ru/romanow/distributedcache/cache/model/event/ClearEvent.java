package ru.romanow.distributedcache.cache.model.event;

import com.google.common.base.MoreObjects;
import org.codehaus.jackson.annotate.JsonIgnore;
import ru.romanow.distributedcache.cache.model.action.ClearAction;

/**
 * Created by ronin on 16.12.15
 */
public class ClearEvent
        extends BaseChangeEvent {
    private static final long serialVersionUID = 7000150338109465377L;

    public ClearEvent() {}

    public ClearEvent(String cacheName) {
        super(cacheName);
    }

    @Override
    @JsonIgnore
    public ClearAction getAction() {
        return new ClearAction();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("cacheName", cacheName)
                          .toString();
    }
}
