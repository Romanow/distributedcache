package ru.romanow.distributedcache.cache.model.action;

import org.springframework.cache.Cache;
import ru.romanow.distributedcache.cache.model.event.PutIfAbsentEvent;

/**
 * Created by ronin on 16.12.15
 */
public class PutIfAbsentAction
        implements Action<PutIfAbsentEvent> {
    @Override
    public void apply(Cache cache, PutIfAbsentEvent changeEvent) {
        cache.putIfAbsent(changeEvent.getKey(), changeEvent.getValue());
    }
}
