package ru.romanow.distributedcache.cache.model.action;

import org.springframework.cache.Cache;
import ru.romanow.distributedcache.cache.model.event.PutEvent;

/**
 * Created by ronin on 16.12.15
 */
public class PutAction
        implements Action<PutEvent> {
    @Override
    public void apply(Cache cache, PutEvent changeEvent) {
        cache.put(changeEvent.getKey(), changeEvent.getValue());
    }
}
