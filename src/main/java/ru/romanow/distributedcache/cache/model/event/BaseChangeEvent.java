package ru.romanow.distributedcache.cache.model.event;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import ru.romanow.distributedcache.cache.model.action.Action;

import java.io.Serializable;

/**
 * Created by ronin on 15.12.15
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public abstract class BaseChangeEvent
        implements Serializable {
    private static final long serialVersionUID = 5913723676356269802L;

    protected String cacheName;

    public BaseChangeEvent() {}

    public BaseChangeEvent(String cacheName) {
        this.cacheName = cacheName;
    }

    public abstract Action getAction();

    public String getCacheName() {
        return cacheName;
    }
}
