package ru.romanow.distributedcache.cache.model.action;

import org.springframework.cache.Cache;
import ru.romanow.distributedcache.cache.model.event.EvictEvent;

/**
 * Created by ronin on 16.12.15
 */
public class EvictAction
        implements Action<EvictEvent> {

    @Override
    public void apply(Cache cache, EvictEvent changeEvent) {
        cache.evict(changeEvent.getKey());
    }
}
