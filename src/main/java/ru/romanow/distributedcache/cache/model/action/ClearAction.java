package ru.romanow.distributedcache.cache.model.action;

import org.springframework.cache.Cache;
import ru.romanow.distributedcache.cache.model.event.ClearEvent;

/**
 * Created by ronin on 16.12.15
 */
public class ClearAction
        implements Action<ClearEvent> {
    @Override
    public void apply(Cache cache, ClearEvent changeEvent) {
        cache.clear();
    }
}
