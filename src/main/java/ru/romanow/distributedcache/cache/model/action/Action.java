package ru.romanow.distributedcache.cache.model.action;

import org.springframework.cache.Cache;
import ru.romanow.distributedcache.cache.model.event.BaseChangeEvent;

/**
 * Created by ronin on 16.12.15
 */
public interface Action<T extends BaseChangeEvent> {
    void apply(Cache cache, T changeEvent);
}
