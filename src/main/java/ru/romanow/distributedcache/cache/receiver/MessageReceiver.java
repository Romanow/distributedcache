package ru.romanow.distributedcache.cache.receiver;

/**
 * Created by ronin on 14.12.15
 */
public interface MessageReceiver<T> {
    void receiveMessage(T obj);
}
