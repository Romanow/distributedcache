package ru.romanow.distributedcache.cache.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import ru.romanow.distributedcache.cache.model.action.Action;
import ru.romanow.distributedcache.cache.model.event.BaseChangeEvent;

/**
 * Created by ronin on 15.12.15
 */
public class CacheChangesReceiver
        implements MessageReceiver<BaseChangeEvent> {

    private static final Logger logger = LoggerFactory.getLogger(CacheChangesReceiver.class);

    private CacheManager cacheManager;

    public CacheManager getCacheManager() {
        return cacheManager;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void receiveMessage(BaseChangeEvent changeEvent) {
        Cache cache = cacheManager.getCache(changeEvent.getCacheName());
        Action action = changeEvent.getAction();

        logger.info("Receive event {}", changeEvent.getClass().getSimpleName());
        action.apply(cache, changeEvent);
    }
}
