package ru.romanow.distributedcache.cache;

import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;
import ru.romanow.distributedcache.cache.storage.CacheStorage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ronin on 05.12.15
 */
public class DistributedCacheManager
        extends AbstractCacheManager {

    private CacheStorage cacheStorage;

    public CacheStorage getCacheStorage() {
        return cacheStorage;
    }

    public void setCacheStorage(CacheStorage cacheStorage) {
        this.cacheStorage = cacheStorage;
    }

    @Override
    protected Collection<? extends Cache> loadCaches() {
        List<Cache> caches = new ArrayList<>();

        if (cacheStorage != null) {
            caches.addAll(
                    cacheStorage.getCacheNames()
                                .stream()
                                .map(this::restoreCache)
                                .collect(Collectors.toList()));
        }

        return caches;
    }

    @Override
    protected Cache getMissingCache(String name) {
        return new DistributedCache(name, cacheStorage);
    }

    private Cache restoreCache(String cacheName) {
        return DistributedCache.restoreCacheState(cacheName, cacheStorage);
    }
}
