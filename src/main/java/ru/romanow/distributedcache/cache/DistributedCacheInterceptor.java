package ru.romanow.distributedcache.cache;

import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.CacheInterceptor;
import org.springframework.context.ApplicationContext;
import ru.romanow.distributedcache.cache.model.event.ClearEvent;
import ru.romanow.distributedcache.cache.model.event.EvictEvent;
import ru.romanow.distributedcache.cache.model.event.PutEvent;
import ru.romanow.distributedcache.cache.publisher.CacheChangesPublisher;

/**
 * Created by ronin on 19.12.15
 */
public class DistributedCacheInterceptor
        extends CacheInterceptor {
    private static final long serialVersionUID = 5881347227235995640L;

    private ApplicationContext applicationContext;
    private CacheChangesPublisher messagePublisher;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        super.setApplicationContext(applicationContext);
        this.applicationContext = applicationContext;
    }

    @Override
    public void afterSingletonsInstantiated() {
        super.afterSingletonsInstantiated();
        this.messagePublisher = this.applicationContext.getBean(CacheChangesPublisher.class);
    }

    @Override
    protected void doPut(Cache cache, Object key, Object result) {
        super.doPut(cache, key, result);
        messagePublisher.publish(new PutEvent(cache.getName(), key.toString(), result));
    }

    @Override
    protected void doEvict(Cache cache, Object key) {
        super.doEvict(cache, key);
        messagePublisher.publish(new EvictEvent(cache.getName(), key.toString()));
    }

    @Override
    protected void doClear(Cache cache) {
        super.doClear(cache);
        messagePublisher.publish(new ClearEvent(cache.getName()));
    }
}
