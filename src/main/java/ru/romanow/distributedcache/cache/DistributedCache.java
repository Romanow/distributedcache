package ru.romanow.distributedcache.cache;

import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;
import ru.romanow.distributedcache.cache.storage.CacheStorage;
import ru.romanow.distributedcache.cache.storage.EmptyCacheStorage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by ronin on 24.11.15
 */
public class DistributedCache
        implements Cache {

    private String cacheName;

    private ConcurrentMap<Object, Object> cache;

    private CacheStorage cacheStorage;

    public DistributedCache(String cacheName) {
        this(cacheName, null);
    }

    public DistributedCache(String cacheName, CacheStorage cacheStorage) {
        this.cache = new ConcurrentHashMap<>();
        this.cacheName = cacheName;

        if (cacheStorage != null) {
            this.cacheStorage = cacheStorage;
        } else {
            this.cacheStorage = new EmptyCacheStorage();
        }
    }

    public static DistributedCache restoreCacheState(String cacheName, CacheStorage cacheStorage) {
        DistributedCache cache = new DistributedCache(cacheName, cacheStorage);
        Map<String, Object> cacheValues = cache.cacheStorage.restoreCacheValues(cacheName);
        cache.cache.putAll(cacheValues);
        return cache;
    }

    @Override
    public String getName() {
        return cacheName;
    }

    @Override
    public Object getNativeCache() {
        return cache;
    }

    @Override
    public ValueWrapper get(Object key) {
        return cache.containsKey(key) ? new SimpleValueWrapper(cache.get(key)) : null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(Object key, Class<T> type) {
        Object value = get(key);
        if (type != null && type.isInstance(value)) {
            return (T)value;
        }
        return null;
    }

    @Override
    public void put(Object key, Object value) {
        cache.put(key, value);
        cacheStorage.putToStorage(cacheName, key.toString(), value);
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        Object cachedValue = cache.putIfAbsent(key, value);
        cacheStorage.putIfAbsentToStorage(cacheName, key.toString(), cachedValue);
        return new SimpleValueWrapper(cachedValue);
    }

    @Override
    public void evict(Object key) {
        cache.remove(key);
        cacheStorage.evictFromStorage(cacheName, key.toString());
    }

    @Override
    public void clear() {
        cache.clear();
        cacheStorage.cleanStorage(cacheName);
    }
}
