package ru.romanow.distributedcache.cache.publisher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import ru.romanow.distributedcache.cache.model.event.BaseChangeEvent;

/**
 * Created by ronin on 15.12.15
 */
public class CacheChangesPublisher
        implements MessagePublisher<BaseChangeEvent> {
    private static final Logger logger = LoggerFactory.getLogger(CacheChangesPublisher.class);

    private ChannelTopic channelTopic;
    private RedisTemplate<String, Object> redisTemplate;

    public ChannelTopic getChannelTopic() {
        return channelTopic;
    }

    public void setChannelTopic(ChannelTopic channelTopic) {
        this.channelTopic = channelTopic;
    }

    public RedisTemplate<String, Object> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void publish(BaseChangeEvent changeEvent) {
        logger.debug("Publish cache change event {}", changeEvent.toString());
        redisTemplate.convertAndSend(channelTopic.getTopic(), changeEvent);
    }
}
