package ru.romanow.distributedcache.cache.publisher;

import ru.romanow.distributedcache.cache.model.event.BaseChangeEvent;

/**
 * Created by ronin on 15.12.15
 */
public interface MessagePublisher<T extends BaseChangeEvent> {
    void publish(T obj);
}
