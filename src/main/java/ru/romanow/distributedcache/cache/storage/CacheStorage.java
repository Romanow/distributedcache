package ru.romanow.distributedcache.cache.storage;

import java.util.Map;
import java.util.Set;

/**
 * Created by ronin on 14.12.15
 */
public interface CacheStorage {
    Set<String> getCacheNames();

    Map<String, Object> restoreCacheValues(String cacheName);

    void putToStorage(String cacheName, String key, Object value);

    void putIfAbsentToStorage(String cacheName, String key, Object value);

    void evictFromStorage(String cacheName, String key);

    void cleanStorage(String cacheName);
}
