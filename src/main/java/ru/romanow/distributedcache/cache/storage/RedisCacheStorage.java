package ru.romanow.distributedcache.cache.storage;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by ronin on 14.12.15
 */
public class RedisCacheStorage
        implements CacheStorage {

    private String storageKeyPrefix;
    private String cacheKeyPrefix;

    private RedisTemplate<String, Object> redisTemplate;
    private HashOperations<String, String, Object> hashOperations;

    public RedisCacheStorage(String storageKeyPrefix, String cacheKeyPrefix) {
        this.storageKeyPrefix = storageKeyPrefix;
        this.cacheKeyPrefix = cacheKeyPrefix;
    }

    public String getStorageKeyPrefix() {
        return storageKeyPrefix;
    }

    public String getCacheKeyPrefix() {
        return cacheKeyPrefix;
    }

    public RedisTemplate<String, Object> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public Set<String> getCacheNames() {
        int beginIndex = storageKeyPrefix.length();
        return redisTemplate.keys(storageKeyPrefix + "*")
                            .stream()
                            .map(name -> name.substring(beginIndex))
                            .collect(Collectors.toSet());
    }

    @Override
    public Map<String, Object> restoreCacheValues(String cacheName) {
        int beginIndex = cacheKeyPrefix.length();
        Map<String, Object> cacheValues = new HashMap<>();
        redisTemplate.<String, Object>opsForHash()
                .entries(storageKeyPrefix + cacheName)
                .forEach((k, v) -> cacheValues.put(k.substring(beginIndex), v));
        return cacheValues;
    }

    @Override
    public void putToStorage(String cacheName, String key, Object value) {
        hashOperations.put(storageKeyPrefix + cacheName, cacheKeyPrefix + key, value);
    }

    @Override
    public void putIfAbsentToStorage(String cacheName, String key, Object value) {
        hashOperations.putIfAbsent(storageKeyPrefix + cacheName, cacheKeyPrefix + key, value);
    }

    @Override
    public void evictFromStorage(String cacheName, String key) {
        hashOperations.delete(storageKeyPrefix + cacheName, cacheKeyPrefix + key);
    }

    @Override
    public void cleanStorage(String cacheName) {
        redisTemplate.delete(storageKeyPrefix + cacheName);
    }
}
