package ru.romanow.distributedcache.cache.storage;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.Map;
import java.util.Set;

/**
 * Created by ronin on 14.12.15
 */
public class EmptyCacheStorage
        implements CacheStorage {

    @Override
    public Set<String> getCacheNames() {
        return Sets.newHashSet();
    }

    @Override
    public Map<String, Object> restoreCacheValues(String cacheName) {
        return Maps.newHashMap();
    }

    @Override
    public void putToStorage(String cacheName, String key, Object value) {}

    @Override
    public void putIfAbsentToStorage(String cacheName, String key, Object value) {}

    @Override
    public void evictFromStorage(String cacheName, String key) {}

    @Override
    public void cleanStorage(String cacheName) {}
}
