package ru.romanow.distributedcache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.AnnotationCacheOperationSource;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheInterceptor;
import org.springframework.cache.interceptor.CacheOperationSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import ru.romanow.distributedcache.cache.DistributedCacheInterceptor;
import ru.romanow.distributedcache.cache.DistributedCacheManager;
import ru.romanow.distributedcache.cache.publisher.CacheChangesPublisher;
import ru.romanow.distributedcache.cache.receiver.CacheChangesReceiver;
import ru.romanow.distributedcache.cache.storage.CacheStorage;
import ru.romanow.distributedcache.cache.storage.RedisCacheStorage;

/**
 * Created by ronin on 24.11.15
 */
@Configuration
@EnableCaching
@PropertySource("classpath:cache.properties")
public class CacheConfiguration {

    @Value("${cache.key.prefix:cache.}")
    private String cacheKeyPrefix;

    @Value("${cache.storage.key.prefix:cache.value.}")
    private String storageKeyPrefix;

    @Bean
    @Autowired
    public CacheManager cacheManager(CacheStorage cacheStorage) {
        DistributedCacheManager distributedCacheManager = new DistributedCacheManager();
//        distributedCacheManager.setCacheStorage(cacheStorage);
        return distributedCacheManager;
    }

    @Bean
    @Autowired
    public CacheStorage cacheStorage(RedisTemplate<String, Object> redisTemplate) {
        RedisCacheStorage redisCacheStorage = new RedisCacheStorage(storageKeyPrefix, cacheKeyPrefix);
        redisCacheStorage.setRedisTemplate(redisTemplate);
        return redisCacheStorage;
    }

    @Bean
    @Autowired
    public CacheChangesPublisher messagePublisher(
            RedisTemplate<String, Object> redisTemplate,
            ChannelTopic channelTopic) {
        CacheChangesPublisher cacheChangesPublisher = new CacheChangesPublisher();
        cacheChangesPublisher.setRedisTemplate(redisTemplate);
        cacheChangesPublisher.setChannelTopic(channelTopic);
        return cacheChangesPublisher;
    }

    @Bean
    @Autowired
    public CacheChangesReceiver messageReceiver(CacheManager cacheManager) {
        CacheChangesReceiver cacheChangesReceiver = new CacheChangesReceiver();
        cacheChangesReceiver.setCacheManager(cacheManager);
        return cacheChangesReceiver;
    }

    @Bean
    public CacheOperationSource cacheOperationSource() {
        return new AnnotationCacheOperationSource();
    }

    @Bean
    @Autowired
    public CacheInterceptor cacheInterceptor(CacheOperationSource cacheOperationSource) {
        DistributedCacheInterceptor distributedCacheInterceptor = new DistributedCacheInterceptor();
        distributedCacheInterceptor.setCacheOperationSources(cacheOperationSource);
        return distributedCacheInterceptor;
    }
}
